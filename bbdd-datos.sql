
INSERT INTO PERSONA VALUES
('53678913P', 'Julián', 'Cabañero', 'Adobe', 'Calle Arriba', 'España', 'ju@gmail.com', '1959-03-03', 'M'), 

('53625323T', 'Sergio', 'Sanz', 'Llamas','Calle Lopez', 'España','san4@gmail.com', '1960-12-25', 'M'), 

('53618912Q', 'Marta', 'Gómez', 'Ruiz', 'Calle Rosal', 'Francia','mar@gmail.com', '1960-03-29', 'F'), 

('53671913O', 'Adrián', 'Llamas', 'Azex', 'Calle Olivo', 'España', 'ad@gmail.com',  '1955-09-15', 'M'), 

('43674513U', 'Montse', 'Faer', 'Beer','Calle Sanz', 'Portugal', 'mont@gmail.com',  '1957-04-05', 'F'), 

('53678913K', 'José', 'Mous', 'Smith', 'Avda.Pepito', 'Reino Unido', 'jose@gmail.com', '1961-05-03', 'M'), 

('53678913M', 'Consuelo', 'Gónzalez', 'Vite', 'Avda.Cal', 'España', 'consu@gmail.com', '1958-07-16', 'F'),  

('65893212L', 'Jesús', 'Fernández', 'Zutra', 'Calle Sol', 'España', 'chule@gmail.com', '1950-11-13', 'M'),

('25986345P', 'Jesús', 'Sánchez', 'Homer', 'Calle Mercurio', 'España', 'chus@gmail.com', '1969-04-20', 'M'), 

('25896316G', 'David', 'Álvarez', 'Bartolo','Calle Veni', 'España','dav@gmail.com', '1998-11-25', 'M'), 

('32168764T', 'Matilde', 'Urdaneta', 'Camarillo', 'Calle Oro', 'Reino Unido', 'Mati@gmail.com', '1961-07-14', 'F'), 

('02359874H', 'Nerea', 'Montero', 'Maben', 'Calle Juan José', 'España', 'nereamo@gmail.com', '2000-08-19', 'F'),  

('56526849S', 'Silvia', 'Espinosa', 'De Rivera', 'Calle Luna', 'Portugal', 'siles@gmail.com', '1950-05-20', 'F'),

('98465135M', 'Laura', 'Bravo', 'Iglesias', 'Avda. Rosales', 'España', 'lauri@gmail.com', '1995-07-22', 'F'), 

('02184658W', 'Maria', 'Peña', 'Gracia','Avda. Amor', 'España','marpe@gmail.com', '1998-12-31', 'F')
;

INSERT INTO TELEFONO VALUES
('675412343', '53678913P'),

('723567132', '53625323T'),

('695412353', '53618912Q'),

('700345123', '53671913O'),

('701224124', '43674513U'),

('645629081', '53678913K'),

('703568621', '53678913M'),

('653983113', '65893212L'),

('678913537', '25986345P'),

('727983412', '25896316G'),

('725423395', '32168764T'),

('671312983', '02359874H'),

('714323423', '56526849S'),

('613421258', '98465135M'),

('643273846', '02184658W')
;

INSERT INTO ALMACEN (direccion) VALUES
('Pepito Perez'),

('Ratoncito Perez'),

('La Elipa'),

('Poeta Blas Otero'),

('Jose Luis Arrese')
;

INSERT INTO EMPLEADO (DNI_PERSONA, sueldo, nºproducto_vende, nºalmacen_almacen) VALUES

('53678913P','700.90', '20', '1'),

('53625323T', '693.00', '10', '3'),

('53618912Q', '500.70', '05', '2'),

('53671913O', '1000.89', '30', '1'),

('43674513U', '2000.00', '15', '3')
;

INSERT INTO DIRIGE VALUES
('53625323T', '53678913P'),

('53678913P', '53618912Q'),

('53618912Q', '53671913O'),

('53625323T', '43674513U')
;

INSERT INTO ESTANTERIA (seccion, nºalmacen_ALMACEN) VALUES
('s', '1'),

('M', '2'),

('C', '4'),

('V', '1'),

('S', '5')
;

INSERT INTO PROVEEDOR (DNI_PERSONA, tipo_transporte) VALUES
('53678913K', 'ferroviario,carretera'),

('53678913M', 'maritimo'),

('65893212L', 'aereo'),

('25986345P', 'carretera'),

('25896316G', 'aereo,carretera')
;

INSERT INTO CLIENTE (DNI_PERSONA, nºcompras, puntos_compras) VALUES
('32168764T','20', '35'),

('02359874H','5', '45'),

('56526849S','10', '36'),

('98465135M','17', '12'),

('02184658W','6', '10')
;


INSERT INTO PEDIDO (forma_pago, metodo_compra, fecha_emision, fecha_entrega, DNI_PERSONA_CLIENTE) VALUES
('transferencia','online', '2017-05-10 20:52:03', '2017-05-20', '32168764T'),

('PayPal', 'online', '2018-06-05 02:06:08', '2018-06-10', '02359874H'),

('contrareembolso','tienda', '2018-10-03 06:45:31', '2018-10-05', '56526849S'),

('efectivo', 'tienda', '2018-11-06 17:25:10', '2018-11-20', '98465135M'),

('tarjeta','online', '2019-01-20 19:08:14', '2018-02-14', '02184658W')
;



INSERT INTO PRODUCTO (fabricante, nombre, cantidad_incluye, unidad_medida, formato, precio, fecha_caducidad, intervalo, nºcomision, nºpedido_PEDIDO, cantidad_total_PEDIDO, DNI_PERSONA_PROVEEDOR, precio_provee, cantidad_provee) VALUES

('Inglot','Base maquillaje', '30', ' ml','liquido','25.95', '24', ' meses abre', '05', '10', '3', '53678913K', '17', '20'),

('Wartheimer', 'Chanel', '150', ' ml', 'liquido','109', '24', ' meses abre','21', '10', '3', '53678913M', '80', '10'),

('Normon','Paroxetina','20', ' mg', 'comprimido', '5.25','meses', '12','10', '12', '4', '65893212L', '3', '30'),

('Combix','Flutox','3.54', 'mg/ml', 'liquido','9.95', '12', ' meses','30', '11', '3', '25986345P', '4', '05'),

('Essence','Barra Labios','1.5', 'g', 'solido','2.99', '12', ' meses abre',  '05', '12', '4', '25896316G', '1', '15'), 

('Victorias Secret','Parfuam', '50', ' ml','liquido','25.95', '24', ' meses abre', '20', '13', '2', '53678913K', '17', '20'),

('Pisa', 'Propofol', '150', ' mg','comprimido','10', '24', 'meses','21', '14', '3', '53678913M', '7', '10'),

('Gemicitabine', 'Paroxetina', '20', ' mg', 'solido', '5.25', '1', 'mes', '15', '11', '3', '65893212L', '3', '20'),

('Divina','Masglo','7', ' ml', 'liquido', '9.95', '2', 'meses','30', '14', '3', '25986345P', '04', '05'),

('Essence','Base maquillaje ultra', '1.5', ' g', 'solido', '2.99', '12', ' meses abre',  '06', '12', '4', '25896316G', '1', '15'), 

('Paclitaxel', 'Fressenius', '30', ' mg','comprimido','25.95', '24', ' meses abre', '05', '10', '3', '53678913K', '27', '11'),

('PSH', 'Intimate','150', ' ml','liquido','35', '12', ' meses abre',   '5', '11', '3', '53678913M', '30', '10'),

('Calvin Clein','Cucumis',  '20', ' cl','liquido', '25.99', '24', 'meses','10', '14', '3', '65893212L', '22.25', '30'),

('Astor','Eyeliner','35', ' ml', 'liquido', '15.99', '4', 'meses', '30', '12', '4', '25986345P', '10', '20'),

('Jafra', 'Zoogar', '105', ' ml', 'liquido', '2.99', '12', ' meses abre',  '05', '13', '2', '25896316G', '1', '12')
;


INSERT INTO MEDICAMENTO (referencia_PRODUCTO, intervalo_toma, dosis, unidad_medida) VALUES
('8','8 horas', '1', 'g'),

('12','2 días', '600', ' mg'),

('11','16 horas', '500', ' mg'),

('7','4 horas', '4', ' mg/ml'),

('15','4 horas', '4', ' mg/ml')
;


INSERT INTO ESTETICA (referencia_PRODUCTO, color, zona) VALUES
('5','carne claro', 'cara '),

('13','rojo', 'uñas'),

('18','verde', 'ojos '),

('14','carne oscuro', 'cara '),

('9','Morado', 'labios')
;

INSERT INTO PERFUME (referencia_PRODUCTO, intensidad, aroma) VALUES
('6','fuerte', 'Vainilla'),

('19','medio', 'Fresa '),

('10','bajo', 'Mandarina'),

('17','fuerte', 'Melón'),

('16','medio', 'Sandía')
;
