DROP DATABASE IF EXISTS FARMACIA;
CREATE DATABASE IF NOT EXISTS FARMACIA;
USE FARMACIA;


CREATE TABLE PERSONA (
DNI CHAR (9),
nombre VARCHAR (20),
apellido1 VARCHAR (20),
apellido2 VARCHAR (20),
direccion VARCHAR (50),
email VARCHAR (50)
);


CREATE TABLE TELEFONO(
telefono_PERSONA CHAR(9),
DNI_PERSONA CHAR(9)
);

CREATE TABLE ALMACEN (
nºalmacen INT,
direccion VARCHAR(50)
);

CREATE TABLE EMPLEADO (
DNI_PERSONA CHAR(9),
sueldo FLOAT,
nºempleado INT,
nºproducto_vende INT,
nºalmacen_ALMACEN INT
);

CREATE TABLE DIRIGE (
jefe_DNI_PERSONA_EMPLEADO CHAR(9),
subordinado_DNI_PERSONA_EMPLEADO CHAR(9)
);

CREATE TABLE ESTANTERIA (
nºestanteria INT,
seccion CHAR,
nºalmacen_ALMACEN INT
);

CREATE TABLE PROVEEDOR (
DNI_PERSONA CHAR(9),
nºproveedor INT,
tipo_transporte VARCHAR(20)
);

CREATE TABLE CLIENTE (
DNI_PERSONA CHAR(9),
nºsocio INT,
nºcompras INT,
puntos_compras INT
);

CREATE TABLE PEDIDO (
nºpedido INT,
forma_pago VARCHAR(30),
fecha_emision DATE,
fecha_entrega DATE,
DNI_PERSONA_CLIENTE CHAR(9)
);

CREATE TABLE PRODUCTO (
referencia INT,
fabricante VARCHAR(20),
nombre VARCHAR(40),
cantidad_incluye varchar(10),
formato VARCHAR(20), 
precio DOUBLE(5,2),
fecha_caducidad varchar(20),
DNI_PERSONA_EMPLEADO CHAR(9),
nºcomision SMALLINT,
nºpedido_PEDIDO INT,
cantidad_total_PEDIDO VARCHAR(20),
DNI_PERSONA_PROVEEDOR CHAR(9),
precio_provee DOUBLE(5,2),
cantidad_provee INT
);

CREATE TABLE MEDICAMENTO (
referencia_PRODUCTO INT,
intervalo_toma VARCHAR(20),
dosis VARCHAR(20)
);

CREATE TABLE ESTETICA (
referencia_PRODUCTO INT,
color VARCHAR(20),
zona VARCHAR(20)
);

CREATE TABLE PERFUME(
referencia_PRODUCTO INT,
intensidad VARCHAR(20),
aroma VARCHAR(20)
);

