
/*
			AÑADIR LAS CLAVES PRINCIPALES DE CADA TABLA 
*/

ALTER TABLE PERSONA
ADD CONSTRAINT PK_PERSONA PRIMARY KEY(DNI);
/*DNI es Primary key ya que no dos personas tienen el mismo DNI*/

ALTER TABLE TELEFONO
ADD CONSTRAINT PK_TELEFONO PRIMARY KEY (DNI_PERSONA, telefono_PERSONA);
/* DNI_PERSONA y telefono_PERSONA son primary key ya que no se pueden repetir*/

ALTER TABLE ALMACEN
ADD CONSTRAINT PK_ALMACEN PRIMARY KEY (nºalmacen);
/*nºalmacen es Primary Key ya que no puee haber mas de un almacen con el mimo numero*/

ALTER TABLE EMPLEADO
ADD CONSTRAINT PK_EMPLEADO PRIMARY KEY (nºempleado);
/*nºempleado es Primary Key ya que no puede haber dos empleados con el mismo numero*/  

ALTER TABLE ESTANTERIA
ADD CONSTRAINT PK_ESTANTERIA PRIMARY KEY(nºestanteria, seccion);
/* nºestanteria y seccion son Primary Key ya que no puede haber dos estanterias con el mismo numero ni en estas haber dos secciones con la misma letra*/

ALTER TABLE PROVEEDOR
ADD CONSTRAINT PK_PROVEEDOR PRIMARY KEY (nºproveedor);
/* nºproveedor es Primary key ya que no puede haber dos proveedores con el mismo numero*/

ALTER TABLE CLIENTE
ADD CONSTRAINT PK_CLIENTE PRIMARY KEY (nºsocio);
/* nºsocio es Primary key ya que no puede haber dos socios con el mismo numero*/

ALTER TABLE PEDIDO
ADD CONSTRAINT PK_PEDIDO PRIMARY KEY (nºpedido);
/* nºpedido es Primary key ya que no puede haber dos pedidos con el mismo numero*/

ALTER TABLE PRODUCTO
ADD CONSTRAINT PK_PRODUCTO PRIMARY KEY (referencia);
/* referencia es Primary key ya que no puede haber dos productos con la misma refrencia*/



/*
				QUITAR LAS PRIMARY KEY
*/


ALTER TABLE PERSONA
DROP PRIMARY KEY;

ALTER TABLE EMPLEADO
DROP PRIMARY KEY;

ALTER TABLE PRODUCTO
DROP PRIMARY KEY;


/*

			RESTABLECIMIENTO DE LAS PRIMARY KEY
*/

ALTER TABLE PERSONA
ADD CONSTRAINT PK_PERSONA PRIMARY KEY(DNI);

ALTER TABLE EMPLEADO
ADD CONSTRAINT PK_EMPLEADO PRIMARY KEY (nºempleado);

ALTER TABLE PRODUCTO
ADD CONSTRAINT PK_PRODUCTO PRIMARY KEY (referencia);



/*
				EDICION DE LAS TABLAS 

*/


/* 		
		Edicion de datos en PERSONA 
*/

/*la columna nombre se cambia a que no puede ser nulo */

ALTER TABLE PERSONA
MODIFY nombre varchar(20)NOT NULL;

/*se cambian de nombre y se pone que no puede ser nulo el primer_apellido y el segundo_apellido  */
ALTER TABLE PERSONA
CHANGE apellido1 primer_apellido VARCHAR(20) NOT NULL;

ALTER TABLE PERSONA
CHANGE apellido2 segundo_apellido VARCHAR(20) NOT NULL;

/*se añade la columna fecha_nacimiento con el tipo date*/
ALTER TABLE PERSONA
ADD fecha_nacimiento DATE;

/*se añade la columna genero se tiene que seleccionar M si es masculino o F si es femenino*/
ALTER TABLE PERSONA
ADD genero ENUM('M', 'F');

/*Se añade la columna pais en la que hay que poner el pais de nacimiento*/
ALTER TABLE PERSONA
ADD COLUMN pais VARCHAR(20) AFTER direccion;


/* 
		Edicion de datos en ALMACEN 
*/

/*se pone que nºalmacen se aumente cada vez que se añade un nuevo registro*/
ALTER TABLE ALMACEN
MODIFY nºalmacen INT AUTO_INCREMENT;


/*
		Edicion de datos en EMPLEADO 
*/

/*se pone que nºempleado se aumente cada vez que se añade un nuevo registro*/
ALTER TABLE EMPLEADO
MODIFY nºempleado INT AUTO_INCREMENT;

/*se pone que DNI_PERSONA es una clave candidata*/
ALTER TABLE EMPLEADO
MODIFY DNI_PERSONA CHAR(9) UNIQUE;


/*
 		Edicion de datos en ESTANTERIA 
*/
/*se pone que nºestanteria se aumente cada vez que se añade un nuevo registro*/
ALTER TABLE ESTANTERIA
MODIFY nºestanteria INT AUTO_INCREMENT;



/*		Edicion de datos en PROVEEDOR 
*/

/*se pone que nºproveedor se aumente cada vez que se añade un nuevo registro, pero que empiece en 20*/
ALTER TABLE PROVEEDOR
MODIFY nºproveedor INT AUTO_INCREMENT,AUTO_INCREMENT = 20 ;

/*Se cambia el tipo de dato de tipo_transporte a poder elegir entre las opciones, y si no se introduce que se ponga por defecto carretera*/
ALTER TABLE PROVEEDOR
MODIFY tipo_transporte SET('carretera', 'ferroviario', 'maritimo', 'aereo') DEFAULT 'carretera';

/*Se pone que DNI_PERSONA es una clave candidata*/
ALTER TABLE PROVEEDOR
MODIFY DNI_PERSONA CHAR(9) UNIQUE;


/* 		Edicion de datos en CLIENTE
*/

/*se pone que nºsocio se aumente cada vez que se añade un nuevo registro, pero que empiece en 31*/
ALTER TABLE CLIENTE
MODIFY nºsocio INT AUTO_INCREMENT, AUTO_INCREMENT = 31;

/*Se pone que DNI_PERSONA es una clave candidata*/
ALTER TABLE CLIENTE
MODIFY DNI_PERSONA CHAR(9) UNIQUE;


/* 		Edicion de datos en PEDIDO 
*/

/* se cambia el tipo de dato de forma_pago a poder elegir entre las opciones, y si no se inroduce que se ponga por defecto tarjeta*/
ALTER TABLE PEDIDO
MODIFY forma_pago ENUM('transferencia', 'efectivo', 'tarjeta', 'PayPal', 'contrareembolso') DEFAULT 'tarjeta';

/* se cambia de tipo a fecha_emision*/
ALTER TABLE PEDIDO
MODIFY fecha_emision TIMESTAMP;

/*se añade la columna metodo_compra, despues de forma_pago, la cual solo puede tener los valores online o tienda y si no se pone nada, por defecto se pone tienda*/
ALTER TABLE PEDIDO
ADD COLUMN metodo_compra ENUM('online', 'tienda') DEFAULT 'tienda' AFTER forma_pago ;

/*se pone que nºpedido se aumente cada vez que se añade un nuevo registro, pero que empiece en 10*/
ALTER TABLE PEDIDO 
MODIFY nºpedido INT AUTO_INCREMENT, AUTO_INCREMENT = 10;




/* 		Edicion de datos en PRODUCTO
*/

/*se pone que referencia se aumente cada vez que se añade un nuevo registro, pero que empiece en 5*/
ALTER TABLE PRODUCTO
MODIFY referencia int AUTO_INCREMENT,AUTO_INCREMENT = 5 ;

/* Se pone  para que el formato solo pueda ser liquido, comprimido o solido*/
ALTER TABLE PRODUCTO
MODIFY formato ENUM('liquido', 'comprimido', 'solido');

/*Se añade la columna unidad_medida despues de canidad_incluye*/
ALTER TABLE PRODUCTO
ADD COLUMN unidad_medida VARCHAR(20) AFTER cantidad_incluye;

/*Se añade la columna intervalo despues de fecha_caducidad*/
ALTER TABLE PRODUCTO
ADD COLUMN intervalo VARCHAR(20) AFTER fecha_caducidad;

/*Se borra la columna DNI_PERSONA_EMPLEADO*/
ALTER TABLE PRODUCTO
DROP DNI_PERSONA_EMPLEADO;	 


/* 		Edicion de datos en MEDICAMENTO
*/

/*Se añade la columna unidad_medida despues de dosis*/
ALTER TABLE MEDICAMENTO
ADD COLUMN unidad_medida VARCHAR(20) AFTER dosis;


/* 		Edicion de datos en ESTETICA
*/

/*Se pone para que en zona solo se pueda poner cara, uñas, ojos o labios*/
ALTER TABLE ESTETICA
MODIFY zona ENUM('cara', 'uñas', 'ojos', 'labios');


/* 		Edicion de datos en PERFUME
*/

/*Se pone para que en intensidad solo se pueda poner fuerte, medio o bajo*/
ALTER TABLE PERFUME
MODIFY intensidad ENUM('fuerte', 'medio', 'bajo');




/*
		AÑADIR LAS FOREIGN KEYS 
*/

/*En las referencias de las foreign key hemos puesto que cuando se borre y cuando se actualice en el campo padre se borre y actualice en todas (CASCADE)*/

/* FOREING KEYS DE PERSONA */
ALTER TABLE TELEFONO
ADD CONSTRAINT FK_TELEFONO FOREIGN KEY (DNI_PERSONA) REFERENCES PERSONA(DNI)  ON DELETE CASCADE ON UPDATE CASCADE;


/* FOREING KEYS DE EMPLEADO */
ALTER TABLE EMPLEADO
ADD CONSTRAINT FK_EMPLEADO_DNI FOREIGN KEY (DNI_PERSONA) REFERENCES PERSONA(DNI)  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE EMPLEADO
ADD CONSTRAINT FK_EMPLEADO_ALMACEN FOREIGN KEY (nºalmacen_ALMACEN) REFERENCES ALMACEN(nºalmacen) ON DELETE CASCADE ON UPDATE CASCADE ;


/* FOREING KEYS DE DIRIGE */
ALTER TABLE DIRIGE
ADD CONSTRAINT FK_DIRIGE_JEFE FOREIGN KEY (jefe_DNI_PERSONA_EMPLEADO) REFERENCES EMPLEADO(DNI_PERSONA) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE DIRIGE
ADD CONSTRAINT FK_DIRIGE_SUBORDINADO FOREIGN KEY (subordinado_DNI_PERSONA_EMPLEADO) REFERENCES EMPLEADO(DNI_PERSONA)  ON DELETE CASCADE ON UPDATE CASCADE;


/* FOREING KEYS DE ESTANTERIA */
ALTER TABLE ESTANTERIA
ADD CONSTRAINT FK_ESTANTERIA FOREIGN KEY (nºalmacen_ALMACEN) REFERENCES ALMACEN(nºalmacen)  ON DELETE CASCADE ON UPDATE CASCADE;


/* FOREING KEYS DE PROVEEDOR */
ALTER TABLE PROVEEDOR
ADD CONSTRAINT FK_PROVEEDOR FOREIGN KEY (DNI_PERSONA) REFERENCES PERSONA(DNI)  ON DELETE CASCADE ON UPDATE CASCADE;


/* FOREING KEYS DE CLIENTE */
ALTER TABLE CLIENTE
ADD CONSTRAINT FK_CLIENTE FOREIGN KEY (DNI_PERSONA) REFERENCES PERSONA(DNI)  ON DELETE CASCADE ON UPDATE CASCADE;


/* FOREING KEYS DE PEDIDO */
ALTER TABLE PEDIDO
ADD CONSTRAINT FK_PEDIDO FOREIGN KEY (DNI_PERSONA_CLIENTE) REFERENCES CLIENTE(DNI_PERSONA)  ON DELETE CASCADE ON UPDATE CASCADE;


/* FOREING KEYS DE PRODUCTO */

ALTER TABLE PRODUCTO
ADD CONSTRAINT FK_PRODUCTO_PEDIDO FOREIGN KEY (nºpedido_PEDIDO) REFERENCES PEDIDO (nºpedido) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE PRODUCTO
ADD CONSTRAINT FK_PRODUCTO_PROVEEDOR FOREIGN KEY (DNI_PERSONA_PROVEEDOR) REFERENCES PROVEEDOR(DNI_PERSONA) ON DELETE CASCADE ON UPDATE CASCADE ;


/* FOREING KEYS DE MEDICAMENTO */
ALTER TABLE MEDICAMENTO
ADD CONSTRAINT FK_MEDICAMENTO FOREIGN KEY (referencia_PRODUCTO) REFERENCES PRODUCTO(referencia) ON DELETE CASCADE ON UPDATE CASCADE ;


/* FOREING KEYS DE ESTETICA */
ALTER TABLE ESTETICA
ADD CONSTRAINT FK_ESTETICA FOREIGN KEY (referencia_PRODUCTO) REFERENCES PRODUCTO(referencia) ON DELETE CASCADE ON UPDATE CASCADE;


/* FOREING KEYS DE PERFUME */
ALTER TABLE PERFUME
ADD CONSTRAINT FK_PERFUME FOREIGN KEY (referencia_PRODUCTO) REFERENCES PRODUCTO(referencia) ON DELETE CASCADE ON UPDATE CASCADE ;



/*

					BORRADO DE LAS FOREIGN KEY
*/

ALTER TABLE PRODUCTO
DROP FOREIGN KEY FK_PRODUCTO_PEDIDO;

ALTER TABLE PROVEEDOR
DROP FOREIGN KEY FK_PROVEEDOR;

ALTER TABLE DIRIGE
DROP FOREIGN KEY FK_DIRIGE_SUBORDINADO;



/*

		RESTABLECIMEITNO DE LAS FOREIGN KEY
*/



ALTER TABLE PRODUCTO
ADD CONSTRAINT FK_PRODUCTO_PEDIDO FOREIGN KEY (nºpedido_PEDIDO) REFERENCES PEDIDO(nºpedido)  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE PROVEEDOR
ADD CONSTRAINT FK_PROVEEDOR FOREIGN KEY (DNI_PERSONA) REFERENCES PERSONA(DNI)  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE DIRIGE
ADD CONSTRAINT FK_DIRIGE_SUBORDINADO FOREIGN KEY (subordinado_DNI_PERSONA_EMPLEADO) REFERENCES EMPLEADO(DNI_PERSONA)  ON DELETE CASCADE ON UPDATE CASCADE;

