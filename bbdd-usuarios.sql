/*Borrado de usuarios si existen */
DROP USER IF EXISTS 'Admin'@'localhost';
DROP USER IF EXISTS 'Empleado'@'localhost';
DROP USER IF EXISTS 'Cliente'@'localhost';
DROP USER IF EXISTS 'Proveedor'@'localhost';

/* Creacion de los usuarios */
CREATE USER 'Admin'@'localhost'IDENTIFIED BY 'Admin';
CREATE USER 'Empleado'@'localhost' IDENTIFIED BY 'Empleado';
CREATE USER 'Cliente'@'localhost' IDENTIFIED BY 'Cliente';
CREATE USER 'Proveedor'@'localhost' IDENTIFIED BY 'Proveedor';

/*Administrador*/
/*Tiene todos los permisos sobre todas las bases de datos.
  También puede crear usuarios , modificar sus permisos y borrarlos. 
*/
GRANT ALL PRIVILEGES ON *.* to 'Admin'@'localhost';
GRANT CREATE USER, GRANT OPTION ON *.* TO 'Admin'@'localhost';

/*Empleado*/
/* Tiene los permisos de actualizar, ver e insertar datos sobre la tabla pedidos */

GRANT UPDATE, SELECT, INSERT ON FARMACIA.PEDIDO TO 'Empleado'@'localhost';

/*Cliente*/
/* Tiene los permisos de actualizar e insertar datos sobre la tabla Cliente */

GRANT INSERT, UPDATE, INSERT ON FARMACIA.CLIENTE TO 'Cliente'@'localhost';

/*Proveedor*/
/* Tiene los permisos de insertar, actualizar y ver los datos en la tabla Pedido */

GRANT UPDATE, SELECT, INSERT ON FARMACIA.PEDIDO TO 'Empleado'@'localhost';




