DROP DATABASE IF EXISTS FARMACIA;
CREATE DATABASE IF NOT EXISTS FARMACIA;
USE FARMACIA;


CREATE TABLE PERSONA (
DNI CHAR (9),
nombre VARCHAR (20),
apellido1 VARCHAR (20),
apellido2 VARCHAR (20),
direccion VARCHAR (50),
pais VARCHAR(20),
email VARCHAR (50), 
fecha_nacimiento date,
genero set('M', 'F')
);


CREATE TABLE TELEFONO(
telefono_PERSONA CHAR(9),
DNI_PERSONA CHAR(9)
);

CREATE TABLE EMPLEADO (
DNI_PERSONA CHAR(9),
sueldo FLOAT,
nºempleado INT,
nºproducto_vende INT,
nºalmacen_ALMACÉN INT
);

CREATE TABLE DIRIGE (
jefe_DNI_PERSONA_EMPLEADO CHAR(9),
subordinado_DNI_PERSONA_EMPLEADO CHAR(9)
);

CREATE TABLE ALMACÉN (
nºalmacen INT,
dirección VARCHAR(50)
);

CREATE TABLE ESTANTERÍA (
nºestanteria INT,
sección CHAR,
nºalmacen_ALMACEN INT
);



CREATE TABLE PROVEEDOR (
DNI_PERSONA CHAR(9),
nºproveedor INT,
tipo_transporte SET('carretera', 'ferroviario', 'marítimo', 'aéreo')
);

CREATE TABLE CLIENTE (
DNI_PERSONA CHAR(9),
nºsocio INT,
nºcompras INT,
puntos_compras INT
);

CREATE TABLE PEDIDO (
nºpedido INT,
forma_pago ENUM('transferencia', 'efectivo', 'tarjeta', 'PayPal', 'contrareembolso')  ,
fecha_emisión DATE,
fecha_entrega DATE,
DNI_PERSONA_CLIENTE CHAR(9)
);

CREATE TABLE PRODUCTO (
referencia INT,
fabricante VARCHAR(20),
nombre VARCHAR(20),
cantidad_incluye INT,
formato ENUM('liquido', 'comprimido', 'solido'),
precio DOUBLE(5,2),
fecha_caducidad DATE,
DNI_PERSONA_EMPLEADO CHAR(9),
nº_comisión SMALLINT,
nº_pedido_PEDIDO INT,
cantidad_total_PEDIDO VARCHAR(20),
DNI_PERSONA_PROVEEDOR CHAR(9),
precio_provee DOUBLE(5,2),
cantidad_provee INT
);

CREATE TABLE MEDICAMENTO (
referencia_PRODUCTO INT,
intervalo_toma VARCHAR(20),
dosis VARCHAR(20)
);

CREATE TABLE ESTÉTICA (
referencia_PRODUCTO INT,
color VARCHAR(20),
zona ENUM('cara', 'uñas', 'ojos', 'labios')
);

CREATE TABLE PERFUME(
referencia_PRODUCTO INT,
intensidad ENUM('fuerte', 'medio', 'bajo'),
aroma VARCHAR(20)
);
